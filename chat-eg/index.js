var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);


app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});
io.on('connection', function(socket){
  console.log('a user connected');
  socket.on('disconnect',function(){
    console.log('user disconnected');
  });
});
io.on('connection', function(socket){
  socket.on('chat message',function(table){
    console.log('message: ' + table);
    console.log(table);
  });
});
io.emit('some event', { someProperty: 'some value', otherProperty: 'other value' }); // This will emit the event to all connected sockets
io.on('connection',function(socket){
  socket.broadcast.emit('hi');
});
io.on('connection', function(socket){
  socket.on('chat message',function(table){
    io.emit('chat message', table);
  });
});
http.listen(3000,function(){
  console.log('listening on *:3000');
});